function createList(array) {
    const elementUl = document.createElement("ul");
    document.body.appendChild(elementUl);

    let elementsLi = array.map(item => {
        let strUl = "";
        if (typeof(item) !== "object") {
            return `<li>${item}</li>`;
        } else {
            if (Array.isArray(item)) {
                for (let value of item) {
                    strUl += `<li>${value}</li>`;
                }
            } else {
                for (let index in item) {
                    strUl += `<li>${item[index]}</li>`;
                }
            }

            return `<ul>${strUl}</ul>`
        }

    });

    for (let li of elementsLi) {
        elementUl.innerHTML += li;
    }

}

createList(["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv", ["NY", "LA", "DC", "CG"], {name: "123"}]);

let button = document.createElement("button");
button.innerText = "Clear";
divElement = document.createElement("div");
document.body.appendChild(divElement);
document.body.appendChild(button);
button.setAttribute("onclick", "clearPage()");


function clearPage() {
    let sec = 3;
    setInterval(tick,1000);
    function tick(){
        divElement.innerText = "left "+(sec--)+" seconds";
    }
    setTimeout(function() {
        document.querySelector("body").outerHTML = "";
    }, 4000);
}



